const express = require('express');
const router = express.Router();
const User = require('../models/user-model');
const UserController = require("../controllers/user-controller");
const { authenticateToken } = require("../utils/api-token");

/* GET single users */
router.get('/:id', UserController.getSingleUser);

/* GET all users */
router.get('/', UserController.getAllUsers);

router.use(authenticateToken)
/* PUT user */
router.put('/:id', UserController.putUser);

/* DELETE user */
router.delete('/:id', UserController.deleteUser);

module.exports = router;
