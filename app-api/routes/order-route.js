const express = require('express');
const router = express.Router();
const Order = require('../models/order-model');
const OrderController = require('../controllers/order-controller');
const { authenticateToken } = require("../utils/api-token");

/* GET order */
router.get('/:id', OrderController.getOrder);

/* GET orders */
router.get('/', OrderController.getOrders);

router.use(authenticateToken)
/* POST order */
router.post('/', OrderController.postOrder);

/* PUT order */
router.put('/:id', OrderController.putOrder);

/* DELETE order */
router.delete('/:id', OrderController.deleteOrder);

module.exports = router;
