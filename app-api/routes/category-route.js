const express = require('express');
const router = express.Router();
const Category = require('../models/category-model');
const CategoryController = require('../controllers/category-controller');
const { authenticateToken } = require("../utils/api-token");

/* GET single category */
router.get('/:id', CategoryController.getSingleCategory);

/* GET all category */
router.get('/', CategoryController.getAllCategories);

router.use(authenticateToken)
/* POST category */
router.post('/', CategoryController.postCategory);

/* PUT category */
router.put('/:id', CategoryController.putCategory);

/* DELETE category */
router.delete('/:id', CategoryController.deleteCategory);

module.exports = router;
