const express = require('express');
const router = express.Router();
const Product = require('../models/product-model');
const ProductController = require('../controllers/product-controller');
const { authenticateToken } = require("../utils/api-token");

/* GET product */
router.get('/:id', ProductController.getProduct);

/* GET products */
router.get('/', ProductController.getProducts);

router.use(authenticateToken)
/* POST product */
router.post('/', ProductController.postProduct);

/* PUT product */
router.put('/:id', ProductController.putProduct);

/* DELETE product */
router.delete('/:id', ProductController.deleteProduct);

module.exports = router;
