const express = require('express');
const router = express.Router();
const Brand = require('../models/brand-model');
const BrandController = require('../controllers/brand-controller');
const { authenticateToken } = require("../utils/api-token");

/* GET single brand */
router.get('/:id', BrandController.getSingleBrand);

/* GET all brand */
router.get('/', BrandController.getAllBrands);

router.use(authenticateToken)
/* POST brand */
router.post('/', BrandController.postBrand);

/* PUT brand */
router.put('/:id', BrandController.putBrand);

/* DELETE brand */
router.delete('/:id', BrandController.deleteBrand);

module.exports = router;
