const express = require('express');
const router = express.Router();
const User = require('../models/user-model');
const AuthController = require('../controllers/auth-controller');

/* POST register */
router.post("/signup", AuthController.register);

/* POST login */
router.post("/signin", AuthController.login);

module.exports = router;
