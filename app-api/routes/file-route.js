const express = require("express");
const router = express.Router();
const upload = require("../middlewares/upload");
const fileController = require("../controllers/file-controller");
const { authenticateToken } = require("../utils/api-token");

/* GET file */
router.get("/:filename", fileController.getFile)

router.use(authenticateToken)
/* POST file */
router.post("/", upload.single("file"), fileController.postFile);

/* DELETE file */
router.delete("/:filename", fileController.deleteFile)

module.exports = router;
