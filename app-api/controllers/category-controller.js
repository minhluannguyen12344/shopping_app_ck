const Category = require("../models/category-model");

/* GET all categories */
exports.getAllCategories = (req, res, next) => {
  Category
  	.find(req.query)
	.populate('parent')
    .exec(async (err, categories) => {
      if (err) return res.status(500).json(err);
      res.json(
        categories.map((item) => {
          return item;
        })
      );
    });
};

/* GET single category */
exports.getSingleCategory = async (req, res, next) => {
  Category
  	.findById({ _id: req.params.id })
	.populate('parent')
    .exec(async (err, result) => {
      if (err) return res.status(500);
	
      if (!result)
        return res.status(404).json({ content: "Data not found!" });

      return res.status(200).json(result);
    }
  );
};

/* POST category */
exports.postCategory = (req, res, next) => {
  const newCategory = new Category(req.body);

  newCategory.save((err, result) => {
    if (err) return res.status(500).json(err);
    return res.status(201).json(result);
  });
};

/* PUT category */
exports.putCategory = (req, res, next) => {
  Category.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
  )
  .exec((err, result) => {
	if (err) return res.status(500).json(err);
	return res.status(200).json(result);
  });
};

/* DELETE category */
exports.deleteCategory = (req, res, next) => {
  Category.findByIdAndDelete({ _id: req.params.id })
	.exec((err, result) => {
	  if (err) return res.status(500).json(err);
	  return res.status(200).json(result);
	})
};
