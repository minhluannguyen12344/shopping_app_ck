const Brand = require("../models/brand-model");

/* GET all brands */
exports.getAllBrands = (req, res, next) => {
  Brand
  	.find(req.query)
    .exec(async (err, brands) => {
      if (err) return res.status(500).json(err);
      res.json(
        brands.map((item) => {
          return item;
        })
      );
    });
};

/* GET single brand */
exports.getSingleBrand = async (req, res, next) => {
  Brand
  	.findById({ _id: req.params.id })
    .exec(async (err, brand) => {
      if (err) return res.status(500).json(err);
	
      if (!brand)
        return res.status(404).json({ content: "Data not found!" });

      return res.status(200).json(brand);
    }
  );
};

/* POST brand */
exports.postBrand = (req, res, next) => {
  const newBrand = new Brand(req.body);

  newBrand.save((err, brand) => {
    if (err) return res.status(500).json(err);
    return res.status(201).json(brand);
  });
};

/* PUT brand */
exports.putBrand = (req, res, next) => {
  Brand.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
  )
  .exec((err, result) => {
	if (err) return res.status(500).json(err);
	return res.status(200).json(result);
  });
};

/* DELETE brand */
exports.deleteBrand = (req, res, next) => {
  Brand.findByIdAndDelete({ _id: req.params.id })
	.exec((err, result) => {
	  if (err) return res.status(500).json(err);
	  return res.status(200).json(result);
	})
};
