const Grid = require("gridfs-stream");
const mongoose = require("mongoose");

let gfs;
const conn = mongoose.connection;
conn.once("open", function () {
  gfs = new Grid(conn.db, mongoose.mongo);
  gfs.collection("files");
});

/* GET file */
exports.getFile = async (req, res, next) => {
  try {
    const file = await gfs.files.findOne({ filename: req.params.filename });
    const readStream = gfs.createReadStream({ filename: file.filename });
    readStream.pipe(res);
  } catch (error) {
    console.log(error);
    res.status(500).json({ content: "Có lỗi xảy ra"});
  }
};

/* POST file */
exports.postFile = async (req, res) => {
    if (req.file === undefined) return res.status(404).json({ content: "Không tìm thấy file"});
    
    return res.status(200).json(req.file)
}

/* DELETE file */
exports.deleteFile = async (req, res) => {
    try {
        await gfs.files.deleteOne({ filename: req.params.filename });
        res.status(200).json({ content: "Xóa thành file công"})
    } catch (error) {
        console.log(error);
        res.status(500).json({ content: "Có lỗi xảy ra"})
    }
};
