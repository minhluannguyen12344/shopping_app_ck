const Order = require("../models/order-model");

/* GET orders */
exports.getOrders = (req, res, next) => {
  const { sort, ...query } = req.query;

  Order
  	.find(query)
	.sort({ createdAt: parseInt(sort) || 1 })  
	.populate('list.product')
    .exec(async (err, orders) => {
      if (err) return res.status(500).json(err);
      res.json(
        orders.map((item) => {
          return item;
        })
      );
    });
};

/* GET order */
exports.getOrder = async (req, res, next) => {
  Order
  	.findById({ _id: req.params.id })
	.populate('list.product')
    .exec(async (err, result) => {
      if (err) return res.status(500);
	
      if (!result)
        return res.status(404).json({ content: "Data not found!" });

      return res.status(200).json(result);
    }
  );
};

/* POST order */
exports.postOrder = (req, res, next) => {
  const newOrder = new Order(req.body);

  newOrder.save((err, result) => {
    if (err) return res.status(500).json(err);
    return res.status(201).json(result);
  });
};

/* PUT order */
exports.putOrder = (req, res, next) => {
  Order.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
  )
  .populate('list.product')
  .exec((err, result) => {
	if (err) return res.status(500).json(err);
	return res.status(200).json(result);
  });
};

/* DELETE order */
exports.deleteOrder = (req, res, next) => {
  Order.findByIdAndDelete({ _id: req.params.id })
  	.populate('list.product')
	.exec((err, result) => {
	  if (err) return res.status(500).json(err);
	  return res.status(200).json(result);
	})
};
