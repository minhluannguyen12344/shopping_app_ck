const User = require("../models/user-model");

/* GET all users */
exports.getAllUsers = (req, res, next) => {
  User
  	.find(req.query)
    .exec(async (err, users) => {
      if (err) return res.status(500).json(err);
      res.json(
        users.map((item) => {
          return item;
        })
      );
    })
};

/* GET single user */
exports.getSingleUser = async (req, res, next) => {
  User
    .findById({ _id: req.params.id })
	.exec((err, user) => {
	  if (!user) return res.status(404).json({ content: "Data not found!" });

	  return res.status(200).json(user);
	})
};

/* PUT user */
exports.putUser = (req, res, next) => {
  User.findOneAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
  )
  .populate("cart")
  .exec((err, result) => {
	if (err) return res.status(500).json(err);
	return res.status(200).json(result);
  });
};

/* DELETE user */
exports.deleteUser = (req, res, next) => {
  User.findByIdAndDelete({ _id: req.params.id })
    .exec((err, result) => {
	  if (err) res.status(500).json(err);
	  return res.status(200).json(result);
    });
};
