const bcrypt = require("bcrypt");
const saltRounds = 10;

const User = require("../models/user-model");
const { generateAccessToken } = require("../utils/api-token");

/* POST register */
exports.register = (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const fullname = req.body.fullname;

  if (!username || !password || !fullname)
    return res.status(500).json({ content: "Missing information!" });

  User.find({ username: username }, function (err, users) {
    if (err) return res.status(500).json(err);
    if (users.length === 1) {
      return res.status(500).json({
        content: "Username existed",
      });
    } else {
      bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {
          const newUser = new User({
			...req.body,
            password: hash,
          });

          newUser.save((err, result) => {
            if (err) return res.status(500).json(err);
			const token = generateAccessToken({ userId: result._id });

            return res.status(201).json({
				data: result,
				token: token
			});
          });
        });
      });
    }
  });
};

/* POST login */
exports.login = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password)
    return res.status(500).json({ content: "Missing username or password!" });

  User.find({ username: username })
	.populate("cart")
	.exec((err, users) => {
		if (err) return res.status(500).json(err);
		if (users.length === 1) {
		const user = users[0];

		bcrypt.compare(password, user.password, async (err, result) => {
			if (result) {
			const token = generateAccessToken({ userId: user._id });
			const findUser = await User.findById(user._id).select("-password")

			return res.json({
				data: findUser,
				token: token
			});
			} else {
			return res.status(401).json({ content: "Invalid username or password!" });
			}
		});
		} else {
		return res.status(401).json({ content: "Invalid username or password!" });
		}
  	});
};