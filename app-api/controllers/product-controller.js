const Product = require("../models/product-model");

const slugGenerator = (str) => {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  const from =
    "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ";
  const to =
    "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
};

/* GET products */
exports.getProducts = (req, res, next) => {
  const { start, limit, search, sort, ...query } = req.query;
  let newQuery = { ...query };
  if (search) newQuery["slug"] = { $regex: search };

  Product.find(newQuery)
  	.sort({ createdAt: parseInt(sort) || 1 })
    .skip(parseInt(start) || 0)
    .limit(parseInt(limit) || 10)
    .populate("brand")
    .populate("category")
    .exec(async (err, products) => {
      if (err) return res.status(500).json(err);
      res.json(
        products.map((item) => {
          return item;
        })
      );
    });
};

/* GET product */
exports.getProduct = async (req, res, next) => {
  Product.findById({ _id: req.params.id })
    .populate("brand")
    .populate("category")
    .exec(async (err, result) => {
      if (err) return res.status(500);

      if (!result) return res.status(404).json({ content: "Data not found!" });

      return res.status(200).json(result);
    });
};

/* POST product */
exports.postProduct = (req, res, next) => {
  const productSlug = slugGenerator(req.body?.name || "");

  Product.findOne({ slug: productSlug }, (err, result) => {
    if (err) return res.status(500).json(err);
    if (result)
      return res.status(501).json({ content: "Vui lòng đặt tên khác !" });

    const newProduct = new Product({
      ...req.body,
      slug: productSlug,
    });
    newProduct.save((err, result) => {
      if (err) return res.status(500).json(err);
      return res.status(201).json(result);
    });
  });
};

/* PUT product */
exports.putProduct = (req, res, next) => {
  Product.findById({ _id: req.params.id })
    .populate("brand")
    .populate("category")
    .exec((err, result) => {
      if (err) return res.status(500).json(err);

      if (req.body?.name) result.slug = slugGenerator(req.body.name);
      Object.assign(result, req.body);

      result.save((err2, res2) => {
        if (err2) return res.status(500).json(err);
        return res.status(200).json(result);
      });
    });
};

/* DELETE product */
exports.deleteProduct = (req, res, next) => {
  Product.findByIdAndDelete({ _id: req.params.id })
    .populate("brand")
    .populate("category")
    .exec((err, result) => {
      if (err) return res.status(500).json(err);
      return res.status(200).json(result);
    });
};
