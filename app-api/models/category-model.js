const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
  name: { type: String, required: true },
  parent: { type: mongoose.Schema.Types.ObjectId, ref: "category" },
  image: { type: String, default:"" },
}, { timestamps: true });

const Category = mongoose.model('category', CategorySchema);

module.exports = Category;
