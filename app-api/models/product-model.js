const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
  slug: { type: String, require: true },
  name: { type: String, required: true },
  price: { type: Number, required: true, default: 0},
  description: { type: String, default: "" },
  available: { type: Number, default: 0 },
  images: [{ type: String, default: "" }],
  brand:  { type: mongoose.Schema.Types.ObjectId, require: true, ref: "brand" },
  category:  { type: mongoose.Schema.Types.ObjectId, require: true, ref: "category" },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "user" }
}, { timestamps: true });

const Product = mongoose.model('product', ProductSchema);

module.exports = Product;
