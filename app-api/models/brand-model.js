const mongoose = require('mongoose');

const BrandSchema = mongoose.Schema({
  name: { type: String, required: true },
  image: { type: String, default: "" }
}, { timestamps: true });

const Brand = mongoose.model('brand', BrandSchema);

module.exports = Brand;
