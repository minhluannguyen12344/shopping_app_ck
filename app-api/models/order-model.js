const mongoose = require('mongoose');
const { required } = require('nodemon/lib/config');

const OrderSchema = mongoose.Schema({
  status: { type: String, required: true },
  list:  [ { type: Object } ],
  total: { type: Number, required: true, default: 0 },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "user" }
}, { timestamps: true });

const Order = mongoose.model('order', OrderSchema);

module.exports = Order;
