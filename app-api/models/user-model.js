const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  fullname: { type: String, required: true },
  role: { type: String, default: "USER" },
  money: { type: Number, required: true, default: 0 },
  avatar: { type: String, default: "" },
  cart: [ { type: mongoose.Schema.Types.ObjectId, ref: "order" } ],
  store: [ { type: mongoose.Schema.Types.ObjectId, ref: "product" } ]
}, { timestamps: true });

const User = mongoose.model('user', UserSchema);

module.exports = User;
