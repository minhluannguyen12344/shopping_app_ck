const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require('cors');

const indexRouter 		= require('./routes/index');
const authRouter		= require('./routes/auth-route');
const userRouter 		= require('./routes/user-route');
const brandRouter 		= require('./routes/brand-route');
const categoryRouter 	= require('./routes/category-route');
const orderRouter 		= require('./routes/order-route');
const productRouter 	= require('./routes/product-route');
const fileRouter 		= require('./routes/file-route');

const app = express();
const server = require("http").createServer(app);

dotenv.config();

mongoose
  .connect(
    process.env.MONGO_HOST,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("DB connect"))
  .catch((err) => console.log(err));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Add headers before the routes are defined
app.use(function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader("Access-Control-Allow-Origin", "*");

	// Request methods you wish to allow
	res.setHeader(
		"Access-Control-Allow-Methods",
		"GET, POST, OPTIONS, PUT, PATCH, DELETE"
	);

	// Request headers you wish to allow
	res.setHeader(
		"Access-Control-Allow-Headers",
		"X-Requested-With,content-type"
	);

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader("Access-Control-Allow-Credentials", true);

	// Pass to next layer of middleware
	next();
});

app.use('/'				, indexRouter);
app.use('/auth'			, authRouter);
app.use('/user' 		, userRouter);
app.use('/brand'		, brandRouter);
app.use('/category'		, categoryRouter);
app.use('/order'		, orderRouter);
app.use('/product'		, productRouter);
app.use('/file'			, fileRouter);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// send back a 404 error for any unknow api request.
app.use((req, res, next) => {
    next(new ApiError(httpStatus.NOT_FOUND, 'Not found'))
})

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.send("Internal Server Error");
});

//port listen
const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
	console.log(`Server is running on http://localhost:${PORT}.`);
});

module.exports = app;

