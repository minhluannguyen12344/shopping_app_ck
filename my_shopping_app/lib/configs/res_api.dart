import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FetchApiRepo {


  Future fetchData(String pathName, Map<String, dynamic> qParams) async {
    dynamic result;
    try {
      Response response = await Dio().get(
        '${HomeUrlApi.baseUrl}$pathName',
        queryParameters: qParams
      );

      result = response.data;
    } on DioError catch (e) {
      // showToastFail(e.response?.data["message"]);
    }
    return result;
  }

  Future postData(String pathName, Map<String, dynamic> data, Map<String, dynamic> header) async {
    dynamic result;
    try {
      Response response = await Dio().post(
        '${HomeUrlApi.baseUrl}$pathName',
        data: data,
        options: Options(
          headers: header
        )
      );

      result = response.data;
    } on DioError catch (e) {
      // showToastFail(e.response?.data["message"]);
      return Fluttertoast.showToast(
          msg: "${e.response!.data["content"]}",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 2,
          backgroundColor: Colors.yellow,
          textColor: Colors.black,
          fontSize: 16.0,
          webPosition: "center",
          webBgColor: "#ffbd40"

        );
    }
    return result;
  }

  Future putData(String pathName, Map<String, dynamic> data) async {
    dynamic result;
    try {
      Response response = await Dio().put(
        '${HomeUrlApi.baseUrl}$pathName',
        data: data,
      );

      result = response.data;
    } on DioError catch (e) {
      // showToastFail(e.response?.data["message"]);
    }
    return result;
  }

  Future postFile(String pathName, dynamic file) async {
    dynamic result;
    try {
      // String fileName = file.path.split('/').last;
      // FormData formData = FormData.fromMap({
      //   "file": await MultipartFile.fromFile(file.path, filename:fileName),
      // });
      Response response = await Dio().post(
        '${HomeUrlApi.baseUrl}$pathName',
        data: file
      );

      result = response.data;
    } on DioError catch(e) {
      // showToastFail(e.response?.data["message"]);
    }
    return result;
  }

}



