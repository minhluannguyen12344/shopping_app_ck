import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Details_Screen.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';

class Product_List_Info extends StatelessWidget {
  final ProductEntity data;

  const Product_List_Info({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          Details_Sceen(products_details: data)));
            },
            child: Container(
              // padding: const EdgeInsets.all(8.0),
              margin: const EdgeInsets.only(top: 4.0, bottom: 4.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.network(
                        '${HomeUrlApi.baseUrl}/file/${data.images![0]}',
                        height: 120,
                        width: 120,
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(width: 16),
                      Flexible(
                        child: Column(
                          children: <Widget>[
                            Text(
                              data.name as String,
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              maxLines: 3,
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            const SizedBox( height: 16 ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Icon(
                                  Icons.attach_money_outlined,
                                  color: Colors.black,
                                  size: 20.0,
                                ),
                                Text(
                                  data.price.toString(),
                                  style: TextStyle(
                                      color: Colors.redAccent.shade700,
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.bold
                                    ),
                                ),
                              ],
                            ),
                          ],
                        ) 
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
  }
}