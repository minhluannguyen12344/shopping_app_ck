import 'package:flutter/material.dart';

class Products {
  final int id;
  final String name;
  final double price;
  final int quantity;
  final String description;
  final String img;

  var images;

  Products({
    required this.id,
    required this.name,
    required this.price,
    required this.quantity,
    required this.description,
    required this.img,
  });
}

List<Products> laps_mobiles = [
  Products(
      id: 1,
      name:
          'Lenovo IdeaPad 1 Laptop, 14.0" HD Display, Intel Celeron N4020, 4GB RAM, 64GB Storage, Intel UHD Graphics 600, Windows 11 in S Mode, Ice Blue ',
      price: 244.99,
      quantity: 10,
      description:
          'This everyday laptop is powered by an Intel Celeron N4020 processor, 4GB DDR4 RAM, and 164 GB M.2 PCIe SSD storage.Microsoft 365 is included (1 year subscription); includes free 3-month trial of Xbox Game Pass Ultimate - Play over 100 high-quality PC games on Windows PC; includes iconic Bethesda games, new day one titles, and the EA Play on PC catalog',
      img: 'assets/img/laptop_4.jpg'),
  Products(
      id: 2,
      name:
          'ASUS Chromebook CX1, 14" Full HD NanoEdge Display, Intel Celeron N3350 Processor, 64GB eMMC Storage, 4GB RAM, Chrome OS, Transparent Silver, CX1400CNA-AS44F',
      price: 224.99,
      quantity: 10,
      description:
          'Powered by the Intel Celeron N3350 Processor 1.1 GHz (2M Cache, up to 2.4 GHz, 2 cores)Durable and built to US Military grade standard MIL- STD 810H weighing just 3.2 lbs',
      img: 'assets/img/laptop_5.jpg'),
  Products(
      id: 3,
      name:
          '2020 Apple MacBook Air Laptop: Apple M1 Chip, 13” Retina Display, 8GB RAM, 256GB SSD Storage, Backlit Keyboard, FaceTime HD Camera, Touch ID. Works with iPhone/iPad; Gold',
      price: 1249.45,
      quantity: 10,
      description:
          'All-Day Battery Life  Go longer than ever with up to 18 hours of battery life.Powerful Performance  Take on everything from professional-quality editing to action-packed gaming with ease. The Apple M1 chip with an 8-core CPU delivers up to 3.5x faster performance than the',
      img: 'assets/img/latop_6.jpg')
];

List<Products> beauty = [
  Products(
      id: 1,
      name:
          'Marcelle BB Cream Beauty Balm, Fair, Hypoallergenic and Fragrance-Free, 1;5 Ounces',
      price: 23.75,
      quantity: 5,
      description:
          'Tinted Moisturizer: Our hydrating beauty balm with self-adjusting pigments is a great alternative to foundation; This multi-tasker hydrates and nourishes while reducing the appearance of imperfections',
      img: 'assets/img/beauty_1.jpg'),
  Products(
      id: 2,
      name:
          'Marcelle Waterproof Lip Definition Crayon, Perfect Rose, Hypoallergenic and Fragrance-Free, 0.04 oz',
      price: 13.00,
      quantity: 5,
      description:
          'Defines lip contour,Prevents lipstick from running,Longlasting, waterproof formula,Matte finish,Blends easily for a natural finish',
      img: 'assets/img/beauty_2.jpg')
];

List<Products> book = [
  Products(
      id: 1,
      name:
          'Atomic Habits: An Easy & Proven Way to Build Good Habits & Break Bad Ones',
      price: 11.98,
      quantity: 5,
      description: '''
            No matter your goals, Atomic Habits offers a proven framework for improving--every day. James Clear, one of the world's leading experts on habit formation,
            reveals practical strategies that will teach you exactly how to form good habits, break bad ones, and master the tiny behaviors that lead to remarkable results.
          ''',
      img: 'assets/img/book_1.jpg'),
  Products(
      id: 2,
      name: 'It Ends with Us: A Novel (1)',
      price: 10.26,
      quantity: 5,
      description: '''
            In this “brave and heartbreaking novel that digs its claws into you and doesnt let go, 
            long after youve finished it” (Anna Todd, New York Times bestselling author) from the #1 
            New York Times bestselling author of All Your Perfects, a workaholic with a too-good-to-be-true 
            romance cant stop thinking about her first love.
          ''',
      img: 'assets/img/book_2.jpg'),
];
List<Products> fashion = [
  Products(
      id: 1,
      name: 'FROGG TOGGS Ultra-Lite2 Waterproof Breathable Poncho',
      price: 12.99,
      quantity: 0,
      description: '''
          100% Other Fibers
          Imported
          Pull On closure
          Machine Wash
          Ultra-Lite2 Rain Poncho includes a packable, waterproof poncho
          Made with Frogg Toggs breathable, non-woven fabric that is waterproof, wind-resistant, and lightweight
          Poncho features an adjustable hood with cord locks and side snaps for extra protection      
      ''',
      img: 'assets/img/fashon_1.jpg'),
];
