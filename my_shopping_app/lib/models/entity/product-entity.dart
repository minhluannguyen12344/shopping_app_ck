import 'dart:convert';
import 'package:my_shopping_app/models/entity/brand-entity.dart';

class ProductEntity {
  static List<ProductEntity> productEntityFromJson(List<dynamic> data) =>
      List<ProductEntity>.from(data.map((x) => ProductEntity.fromJson(x)));

  static String productEntityToJson(List<dynamic> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  ProductEntity({
    this.price,
    this.description,
    this.available,
    this.images,
    this.id,
    this.name,
    this.brand,
    this.category,
    this.createdBy,
    this.slug,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  double? price;
  String? description;
  int? available;
  List<String>? images;
  String? id;
  String? name;
  BrandEntity? brand;
  Category? category;
  String? createdBy;
  String? slug;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory ProductEntity.fromJson(Map<String, dynamic> json) => ProductEntity(
        price: json["price"].toDouble(),
        description: json["description"],
        available: json["available"],
        images: List<String>.from(json["images"].map((x) => x)),
        id: json["_id"],
        name: json["name"],
        brand: BrandEntity.fromJson(json["brand"]),
        category: Category.fromJson(json["category"]),
        createdBy: json["createdBy"],
        slug: json["slug"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "price": price,
        "description": description,
        "available": available,
        "images": List<dynamic>.from(images!.map((x) => x)),
        "_id": id,
        "name": name,
        "brand": brand!.toJson(),
        "category": category!.toJson(),
        "createdBy": createdBy,
        "slug": slug,
        "createdAt": createdAt!.toIso8601String(),
        "updatedAt": updatedAt!.toIso8601String(),
        "__v": v,
    };
}

class Category {
  static List<Category> categoryEntityFromJson(List<dynamic> data) =>
      List<Category>.from(data.map((x) => Category.fromJson(x)));

  static String categoryEntityToJson(List<Category> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  Category({
    this.image,
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.parent,
  });

  String? image;
  String? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;
  String? parent;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        image: json["image"],
        id: json["_id"],
        name: json["name"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        parent: json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
        "_id": id,
        "name": name,
        "createdAt": createdAt!.toIso8601String(),
        "updatedAt": updatedAt!.toIso8601String(),
        "__v": v,
        "parent": parent,
      };
}
