import 'dart:convert';

class BrandEntity {
  static List<BrandEntity> brandEntityFromJson(List<dynamic> data) =>
    List<BrandEntity>.from(data.map((x) => BrandEntity.fromJson(x)));

  static String brandEntityToJson(List<BrandEntity> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  BrandEntity({
    this.image,
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String? image;
  String? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory BrandEntity.fromJson(Map<String, dynamic> json) => BrandEntity(
    image: json["image"],
    id: json["_id"],
    name: json["name"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "image": image,
    "_id": id,
    "name": name,
    "createdAt": createdAt!.toIso8601String(),
    "updatedAt": updatedAt!.toIso8601String(),
    "__v": v,
  };
}
