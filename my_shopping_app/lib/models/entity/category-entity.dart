import 'dart:convert';

class CategoryEntity {
  static List<CategoryEntity> categoryEntityFromJson(List<dynamic> data) =>
      List<CategoryEntity>.from(data.map((x) => CategoryEntity.fromJson(x)));

  static String categoryEntityToJson(List<CategoryEntity> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  CategoryEntity({
    this.image,
    this.id,
    this.name,
    this.parent,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String? image;
  String? id;
  String? name;
  dynamic parent;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory CategoryEntity.fromJson(Map<String, dynamic> json) => CategoryEntity(
    image: json["image"],
    id: json["_id"],
    name: json["name"],
    parent: json["parent"] == null ? null : CategoryEntity.fromJson(json["parent"]),
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "image": image,
    "_id": id,
    "name": name,
    "parent": parent == null ? null : parent!.toJson(),
    "createdAt": createdAt!.toIso8601String(),
    "updatedAt": updatedAt!.toIso8601String(),
    "__v": v,
  };
}
