import 'dart:convert';

import 'package:my_shopping_app/models/entity/product-entity.dart';

class OrderEntity {
  static List<OrderEntity> orderEntityFromJson(List<dynamic> data) =>
      List<OrderEntity>.from(data.map((x) => OrderEntity.fromJson(x)));

  static String orderEntityToJson(List<OrderEntity> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  OrderEntity({
    this.list,
    this.total,
    this.id,
    this.status,
    this.createdBy,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  List<ProductEntity>? list;
  double? total;
  String? id;
  String? status;
  String? createdBy;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory OrderEntity.fromJson(Map<String, dynamic> json) => OrderEntity(
        list: List<ProductEntity>.from(
            json["list"].map((x) => ProductEntity.fromJson(x))),
        total: json["total"],
        id: json["_id"],
        status: json["status"],
        createdBy: json["createdBy"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list!.map((x) => x.toJson())),
        "total": total,
        "_id": id,
        "status": status,
        "createdBy": createdBy,
        "createdAt": createdAt!.toIso8601String(),
        "updatedAt": updatedAt!.toIso8601String(),
        "__v": v,
      };
}
