import 'package:flutter/material.dart';

import 'Screens/Auth_Screen/Login_Screen/login_screen.dart';
import 'Screens/Auth_Screen/Register_Screen/register_screen.dart';
import 'Screens/Products_Screen/Add_products.dart';
import 'Screens/Products_Screen/Default_Screen.dart';
import 'Screens/Products_Screen/components/search_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: Colors.grey.shade200),
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const Default_Screen(),
        '/sign-in': (context) => const Login_Screen(),
        '/sign-up': (context) => const Register_Screen(),
        '/Add_products': (context) => const Add_Products_Screen(),
      },
      initialRoute: '/',
    );
  }
}
