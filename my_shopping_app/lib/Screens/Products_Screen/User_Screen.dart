import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:my_shopping_app/Screens/Products_Screen/components/Order_List.dart';

class User_Screen extends StatefulWidget {
  const User_Screen({Key? key}) : super(key: key);

  @override
  State<User_Screen> createState() => _User_ScreenState();
}

class _User_ScreenState extends State<User_Screen> {
  final LocalStorage storage = LocalStorage('localstorage_app');

  @override
  var thebody = '1';
  var isSignIn = false;
  Widget build(BuildContext context) {
    Widget? changepage;

    Future<void> _CheckSignIn() async {
      await storage.ready;
      String? token = storage.getItem('token');
      Map<String, dynamic>? userInfo = storage.getItem('user-info');
      if (token != null && userInfo != null) {
        return setState(() {
          isSignIn = true;
        });
      }
    }

    _CheckSignIn();

    void _LogOut() async {
      await storage.ready;
      storage.deleteItem('token');
      storage.deleteItem('user-info');
      storage.deleteItem('cart');
      Navigator.pushNamed(context, '/');
    }

    String getUserId() {
      Map<String, dynamic>? userInfo = storage.getItem('user-info');
      return userInfo!["_id"];
    }


    Widget Before_Sign() {
      return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Container(
            padding: EdgeInsets.all(12.0),
            child: Column(
              children: [
                const Text(
                  'Log In For The Best Experience',
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 30.0,
                ),
                SizedBox(
                  height: 50,
                  width: 300,
                  child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/sign-in');
                      },
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.yellow.shade700,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      child: const Text('Log In')),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                SizedBox(
                  height: 50,
                  width: 300,
                  child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/sign-up');
                      },
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.yellow.shade700,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      child: const Text('Sign Up')),
                ),
                const SizedBox(
                  height: 30.0,
                ),
                Container(
                  padding: const EdgeInsets.all(10.0),
                  width: 350,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Image(
                        image: AssetImage('assets/img/checkbox.png'),
                        width: 100,
                        height: 100,
                        fit: BoxFit.fill,
                      ),
                      Flexible(
                        child: Text(
                          'Check order status and track,Change or return items',
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10.0),
                  width: 350,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Image(
                        image: AssetImage('assets/img/bag4.png'),
                        width: 100,
                        height: 100,
                        fit: BoxFit.fill,
                      ),
                      Flexible(
                        child: Text(
                          'Shop fast purchases and every day essentials',
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 350,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Image(
                        image: AssetImage('assets/img/list3.png'),
                        width: 100,
                        height: 100,
                        fit: BoxFit.fill,
                      ),
                      Flexible(
                        child: Text(
                          'Create list with items you want',
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }

    Widget After_Sign() {
      return SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/Add_products');
                        },
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.yellow.shade700,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                        child: const Text('Sell your products ?')),
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: TextButton(
                        onPressed: _LogOut,
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.yellow.shade700,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                        child: const Text('LogOut')),
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Your Order',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Order_List(userId: getUserId())
          ],
        ),
      );
    }

    switch (thebody) {
      case '1':
        changepage = Before_Sign();
        break;
      case '2':
        changepage = After_Sign();
        break;
    }
    return Scaffold(
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.yellow.shade700,
          title: Container(
            padding: const EdgeInsets.all(8.0),
            child: const Text('User'),
          ),
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.search,
                )),
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.mic,
                ))
          ],
        ),
        body: Container(child: isSignIn ? After_Sign() : Before_Sign()));
  }
}
