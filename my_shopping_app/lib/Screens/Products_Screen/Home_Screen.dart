import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Beauty_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Book_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Fashion_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Laptop_Mobile_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Result_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/components/Home_Product_By_Category.dart';
import 'package:my_shopping_app/Screens/Products_Screen/components/search_screen.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:my_shopping_app/models/entity/category-entity.dart';

class Home_Screen extends StatefulWidget {
  const Home_Screen({Key? key}) : super(key: key);

  @override
  _Home_Screen_State createState() => _Home_Screen_State();
}

class _Home_Screen_State extends State<Home_Screen> {
  List<CategoryEntity> data = [];

  @override
  void initState() {
    super.initState();

    FetchApiRepo().fetchData('/category', {}).then((value) => {
          if (mounted)
            {
              setState(() {
                data = CategoryEntity.categoryEntityFromJson(value);
              })
            }
        });
  }

  // List<CategoryEntity> getMainCategory() {
  //   List<CategoryEntity> result = [];
  //   result.addAll(data.where((d) => d.parent == null));
  //   return result;
  // }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    final products_title = ['Laptops & Phone', 'Beauty', 'Books', 'Fashon'];
    final products_picture = [
      'assets/img/laptop&mobile.png',
      'assets/img/beauty.png',
      'assets/img/book.jpg',
      'assets/img/fashon.png'
    ];
    final Categories_Screen = [
      '/Laps&mobile',
      '/beauty',
      '/book',
      '/fashon',
    ];
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 45.0,
              color: Colors.yellow[700],
              padding: const EdgeInsets.all(6.0),
              child: Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: TextField(
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.only(left: 6.0, right: 6.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: const TextStyle(
                              color: Colors.grey, fontSize: 15.0),
                          prefixIcon: const Align(
                            widthFactor: 1.0,
                            heightFactor: 1.0,
                            child: Icon(
                              Icons.search,
                              color: Colors.black,
                              size: 25.0,
                            ),
                          ),
                          hintText: 'Search',
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, '/search');
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 20),
                  Icon(Icons.mic, color: Colors.white),
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 30,
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    decoration: BoxDecoration(color: Colors.yellow.shade600),
                    child: Row(
                      children: const [
                        Icon(Icons.location_on_outlined),
                        Text(
                          'Delivery to VietNam',
                          style: TextStyle(fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                  const Padding(padding: EdgeInsets.all(4.0)),
                  Category_List(data: data),
                  const Image(
                      image: AssetImage('assets/img/advertisement.jpg')),
                  Home_Product_Category(data: data)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Category_List extends StatelessWidget {
  final List<CategoryEntity> data;

  const Category_List({Key? key, required this.data}) : super(key: key);

  List<CategoryEntity> getMainCategory() {
    List<CategoryEntity> result = [];
    result.addAll(data.where((d) => d.parent == null));
    return result;
  }

  List<String> getSubCategory(mainCateId) {
    List<String> result = [];
    result.addAll(data
        .where((d) => d.parent?.id == mainCateId)
        .map((item) => item.id as String)
        .toList());
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      height: MediaQuery.of(context).size.height * 0.24,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: getMainCategory().length,
          itemBuilder: (context, index) {
            final CategoryEntity category = getMainCategory()[index];
            return GestureDetector(
              onTap: (() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Result_Screen(query: {
                              'category': getSubCategory(category.id),
                              'sort': '-1'
                            })));
              }),
              child: Container(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Text(
                      category.name.toString(),
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    const Padding(padding: EdgeInsets.all(4.0)),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image.network(
                        '${HomeUrlApi.baseUrl}/file/${category.image}',
                        height: 100,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
