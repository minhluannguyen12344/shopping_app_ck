import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Details_Screen.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';
import 'package:slugify/slugify.dart';

class SearchScreen extends StatefulWidget {
  static const route = '/search';
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<ProductEntity> searchSuggestions = [];
  TextEditingController mysearch = TextEditingController();


  Future<void> getSearchResult(search) async {
    String slug = slugify(search);
    Map<String, dynamic> qParams = {
      'search': slug,
      'start': '0',
      'limit': '10',
    };
    FetchApiRepo()
      .fetchData('/product', qParams)
      .then((value) => {
        if(mounted){
          setState(() {
            searchSuggestions = ProductEntity.productEntityFromJson(value);
          })
        }
      }
    );
  }

  void clearSuggest() {
    setState(() {
      searchSuggestions = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> suggestions = ["Macbook", "Iphone", "Asus", "Acer"];

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
          backgroundColor: Colors.grey[200],
          body: Column(
            children: [
              Container(
                height: 60.0,
                color: Colors.yellow[700],
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: TextField(
                        controller: mysearch,
                        onChanged: (content) {
                          
                          if (content.length > 2) {
                            getSearchResult(content);
                          } 
                          else {
                            clearSuggest();
                          }
                        },
                        textAlignVertical: TextAlignVertical.center,
                        cursorColor: Colors.black,
                        cursorWidth: 3,
                        cursorHeight: 20,
                        decoration: InputDecoration(
                          icon: IconButton(
                            icon: Icon(Icons.arrow_back,
                                color: Colors.white, size: 20),
                            onPressed: () {
                              Navigator.pushNamed(context, '/');
                            },
                          ),
                          contentPadding:
                              const EdgeInsets.only(left: 6.0, right: 6.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: const TextStyle(
                              color: Colors.grey, fontSize: 15.0),
                          prefixIcon: const Align(
                            widthFactor: 1.0,
                            heightFactor: 1.0,
                            child: Icon(
                              Icons.search,
                              color: Colors.black,
                              size: 20.0,
                            ),
                          ),
                          hintText: 'Search for product name',
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    Icon(Icons.mic, color: Colors.white),
                  ],
                ),
              ),
              Expanded(
                child:
                  ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: searchSuggestions.length,
                      itemBuilder: (context, index) => GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Details_Sceen(products_details: searchSuggestions[index])));
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                border: Border(
                                  bottom:
                                      BorderSide(width: 1, color: Colors.black),
                                ),
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          searchSuggestions[index].name!,
                                          style: const TextStyle(fontSize: 15),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 5),
                                  const Icon(
                                    Icons.north_west_outlined,
                                    size: 25,
                                    color: Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          ))

              )
            ],
          )),
    );
  }
}
