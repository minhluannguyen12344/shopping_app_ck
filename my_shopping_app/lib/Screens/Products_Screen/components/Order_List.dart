import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:my_shopping_app/Screens/Products_Screen/components/Order_Detail.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/models/entity/order-entity.dart';

class Order_List extends StatefulWidget {
  final String userId;
  const Order_List({super.key, required this.userId});

  @override
  State<Order_List> createState() => _Order_ListState();
}

class _Order_ListState extends State<Order_List> {
  List<OrderEntity> data = [];

  @override
  void initState() {
    super.initState();
    Map<String, dynamic> query = {
      "createdBy": widget.userId, 
      "sort": -1
    };
    FetchApiRepo().fetchData('/order', query).then((value) => {
          if (mounted)
            {
              setState(() {
                data = OrderEntity.orderEntityFromJson(value);
              })
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: data.length,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (() {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          Order_Detail(data: data[index])));
            }),
            child: Container(
              margin: const EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Date: ${DateFormat('dd/MM/yyyy').format(data[index].createdAt!)}'),
                  Text('Order ID: ${data[index].id}'),
                  Text('Total: \$${data[index].total}')
                ],
              ),
            ),
          );
        });
  }
}
