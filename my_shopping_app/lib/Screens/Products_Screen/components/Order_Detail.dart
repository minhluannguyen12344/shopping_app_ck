import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:my_shopping_app/components/Product_List_Info.dart';
import 'package:my_shopping_app/models/entity/order-entity.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';

class Order_Detail extends StatelessWidget {
  final OrderEntity data;
  const Order_Detail({super.key, required this.data});

  renderProductList(List<ProductEntity> data) {
    List<dynamic> result = [];
    for (var i = 0; i < data.length; i++) {
      ProductEntity product = data[i];
      int index = result.indexWhere((element) => element["product"].id ==  product.id);
      if(index > -1){
        result[index]["quantity"] += 1;
      } else {
        result.add({
          "product": product,
          "quantity": 1
        });
      }
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Order Detail'),
        backgroundColor: Colors.yellow.shade700
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: Column( 
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox( height: 10),
                  Text(
                    "Total: \$ ${data.total}",
                    style: const TextStyle(
                      color: Colors.red,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.all(8.0),
              itemCount: renderProductList(data.list!).length,
              itemBuilder: (context, index) {
                final dynamic renderData = renderProductList(data.list!)[index];
                final ProductEntity product = renderData["product"];

                return Column(
                  children: [
                    Product_List_Info(data: product),
                    Text(
                      'Qty: ${renderData["quantity"]}',
                      textAlign: TextAlign.right,
                      style: const TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(height: 30)
                  ]
                );
              }
            ),
          ]
        )
      )
    );
  }
}
