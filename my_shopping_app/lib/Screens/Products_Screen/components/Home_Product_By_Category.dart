import 'package:flutter/material.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Result_Screen.dart';
import 'package:my_shopping_app/components/Product_List_Info.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:my_shopping_app/models/entity/category-entity.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';

class Home_Product_Category extends StatefulWidget {
  final List<CategoryEntity> data;

  const Home_Product_Category({Key? key, required this.data}) : super(key: key);

  @override
  _Home_Product_Category_State createState() => _Home_Product_Category_State();
}

class _Home_Product_Category_State extends State<Home_Product_Category> {
  List<CategoryEntity> getMainCategory() {
    List<CategoryEntity> result = [];
    result.addAll(widget.data.where((d) => d.parent == null));
    return result;
  }

  List<String> getSubCategory(mainCateId) {
    List<String> result = [];
    result.addAll(widget.data
        .where((d) => d.parent?.id == mainCateId)
        .map((item) => item.id as String)
        .toList());
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: getMainCategory().length,
            itemBuilder: (context, index) {
              final category = getMainCategory()[index];
              return Container(
                  margin: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Home_Category_Thumbnail(data: category, cateIds: getSubCategory(category.id)),
                      Home_Category_Popular(cateIds: getSubCategory(category.id), title: category.name as String )
                    ],
                  ));
            }));
  }
}

class Home_Category_Popular extends StatefulWidget {
  final List<String> cateIds;
  final String title;

  const Home_Category_Popular({Key? key, required this.cateIds, required this.title}) : super(key: key);

  @override
  _Home_Category_Popular_State createState() => _Home_Category_Popular_State();
}

class _Home_Category_Popular_State extends State<Home_Category_Popular> {
  List<ProductEntity> productData = [];

  @override
  void initState() {
    super.initState();
    if(widget.cateIds.isNotEmpty){
      Map<String, dynamic> qParams = {
        'category': widget.cateIds,
        'start': '0',
        'limit': '3',
        'sort': '-1'
      };
      FetchApiRepo()
        .fetchData('/product', qParams)
        .then((value) => {
          if(mounted){
            setState(() {
              productData = ProductEntity.productEntityFromJson(value);
            })
          }
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(color: Colors.white),
      margin: const EdgeInsets.only(top: 4, bottom: 4),
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${widget.title} newest items',
            style: const TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w500
            ),
          ),
          const SizedBox(height: 12),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: productData.length,
            itemBuilder: (context, index) {
              final ProductEntity data = productData[index];
              return Container(
                margin: const EdgeInsets.only(top: 8, bottom: 8),
                child: Product_List_Info(data: data)
              );
            }
          )
        ],
      ),
    );
  }
}

class Home_Category_Thumbnail extends StatelessWidget {
  final CategoryEntity data;
  final List<String> cateIds;

  const Home_Category_Thumbnail({Key? key, required this.data, required this.cateIds}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (() {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Result_Screen(query: {
                'category': cateIds,
                'sort': '-1'
              })
            )
          );
        }),
        child: Container(
            margin: const EdgeInsets.only(top: 4, bottom: 4),
            decoration: const BoxDecoration(color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  data.name.toString(),
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500
                  ),
                ),
                const SizedBox(width: 20),
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(50.0),
                      bottomLeft: Radius.circular(50.0)),
                  child: Image.network(
                    '${HomeUrlApi.baseUrl}/file/${data.image}',
                    height: 110,
                    width: 150,
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            )));
  }
}
