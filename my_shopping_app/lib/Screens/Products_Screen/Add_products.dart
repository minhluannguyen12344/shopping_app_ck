import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import 'package:form_builder_image_picker/form_builder_image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:my_shopping_app/models/entity/brand-entity.dart';
import 'package:my_shopping_app/models/entity/category-entity.dart';

class Add_Products_Screen extends StatefulWidget {
  const Add_Products_Screen({Key? key}) : super(key: key);

  @override
  State<Add_Products_Screen> createState() => _Add_Products_ScreenState();
}

class _Add_Products_ScreenState extends State<Add_Products_Screen> {
  final LocalStorage storage = LocalStorage('localstorage_app');
  List<BrandEntity> Brand_list = [];
  List<CategoryEntity> Category_list = [];

  @override
  void initState() {
    super.initState();
    FetchApiRepo().fetchData('/brand', {}).then((value) => {
          if (mounted)
            {
              setState(() {
                Brand_list = BrandEntity.brandEntityFromJson(value);
              })
            }
        });
    FetchApiRepo().fetchData('/category', {}).then((value) => {
          if (mounted)
            {
              setState(() {
                Category_list = CategoryEntity.categoryEntityFromJson(value);
              })
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormBuilderState>();
    List<String> doubleList =
        List<String>.generate(50, (int index) => '${index + 1}');

    uploadImage(File image) async {
      var dio = Dio();
      dio.post("${HomeUrlApi.baseUrl}/file",
          data: image.openRead(),
          options: Options(
              contentType: "image/png",
              headers: {"Content-Length": image.lengthSync()}));
    }

    postProduct(data) async {
      await storage.ready;
      String? token = storage.getItem('token');
      List<String> images = [];
      List<dynamic> imagesInput = data["images"];
      for (var i = 0; i < imagesInput.length; i++) {
        File file = File(imagesInput[i].path);
        String fileName = file.path.split('/').last;

        FormData formData = FormData.fromMap({
          "file": await MultipartFile.fromFile(file.path, filename: fileName),
        });
        await FetchApiRepo().postFile("/file", formData).then((value) => {
              images.add(value["filename"]),
            });
      }

      Map<String, dynamic> body = {...data, "images": images};

      FetchApiRepo().postData("/product", body, {
        "Authorization": "Bearer: ${token as String}"
      }).then((value) => {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Add success')),
            ),
            _formKey.currentState!.reset()
          });
    }

    filterCategory(List<CategoryEntity> data) {
      return data.where((element) => element.parent != null);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Add product'),
        backgroundColor: Colors.yellow.shade700,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: FormBuilder(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderTextField(
                      name: 'name',
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter name';
                        }
                        return null;
                      },
                      obscureText: false,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(10.0),
                        hintText: 'Product name',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderTextField(
                      name: 'price',
                      obscureText: false,
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        RegExp regExp = RegExp(r'[+]?([0-9]*\.[0-9]+|[0-9]+)');
                        if (value == null || value.isEmpty) {
                          return 'Please enter product price';
                        }
                        if (regExp.hasMatch(value)) {
                          return null;
                        } else {
                          return 'please enter right format';
                        }
                      },
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(10.0),
                        hintText: 'Price',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderTextField(
                      name: 'description',
                      obscureText: false,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter description';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(10.0),
                        hintText: 'Description',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(16.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderDropdown(
                      name: 'available',
                      hint: Text('Select quantity'),
                      isExpanded: true,
                      items: doubleList.map((quantity) {
                        return DropdownMenuItem(
                          child: Text("$quantity"),
                          value: quantity,
                        );
                      }).toList(),
                      validator: (value) {
                        if (value == null) {
                          return 'Please pick a quantity';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(16.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderDropdown(
                      name: 'brand',
                      hint: Text('Select brand'),
                      isExpanded: true,
                      items: Brand_list.map((brand) {
                        return DropdownMenuItem(
                          child: Text("${brand.name}"),
                          value: brand.id,
                        );
                      }).toList(),
                      validator: (value) {
                        if (value == null) {
                          return 'Please pick a brand';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(16.0),
                  child: SizedBox(
                    width: 500.0,
                    child: FormBuilderDropdown(
                      name: 'category',
                      hint: Text('Select Category'),
                      isExpanded: true,
                      items: filterCategory(Category_list).map((item) {
                        return DropdownMenuItem(
                          child: Text("${item.name}"),
                          value: item.id,
                        );
                      }).toList(),
                      validator: (value) {
                        if (value == null) {
                          return 'Please pick a Category';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
              ),
              Center(
                child: FormBuilderImagePicker(
                  name: 'images',
                  decoration: const InputDecoration(labelText: 'Pick img'),
                  maxImages: 5,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter product name';
                    }
                    if (value.length > 5) {
                      return 'To many img ,just 5';
                    }
                    return null;
                  },
                ),
              ),
              Center(
                child: SizedBox(
                  height: 50,
                  width: 150,
                  child: TextButton(
                      onPressed: () {
                        _formKey.currentState!.save();
                        if (_formKey.currentState!.validate()) {
                          postProduct(_formKey.currentState!.value);
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Add  fail')));
                        }
                      },
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.yellow.shade700,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      child: const Text('Add')),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
