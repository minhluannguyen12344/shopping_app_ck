import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:localstorage/localstorage.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/configs/url_api.dart';

import '../../models/entity/product-entity.dart';
import 'Categories_Screen/Details_Screen.dart';

class Cart_Screen extends StatefulWidget {
  const Cart_Screen({
    Key? key,
  }) : super(key: key);
  @override
  State<Cart_Screen> createState() => _Cart_ScreenState();
}

class _Cart_ScreenState extends State<Cart_Screen> {
  final LocalStorage storage = LocalStorage('localstorage_app');
  List<ProductEntity> cart_data = [];
  bool isSignIn = false;

  Future<void> _CheckSignIn() async {
    await storage.ready;
    String? token = storage.getItem('token');
    Map<String, dynamic>? userInfo = storage.getItem('user-info');
    if(token != null && userInfo != null){
      return setState(() {
        isSignIn = true;
      });
    }
  }

  getCart() async {
    await storage.ready;
    List<ProductEntity> data = ProductEntity.productEntityFromJson(
        jsonDecode(storage.getItem('cart') ?? "[]"));
    setState(() {
      cart_data = data;
    });
  }

  addProductQuantity(product) {
    List<ProductEntity> productList =
        cart_data.where((element) => element.id == product.id).toList();
    if (productList.length < product.available) {
      List<ProductEntity> updateList = cart_data;
      updateList.add(product);
      storage.setItem('cart', ProductEntity.productEntityToJson(updateList));
      setState(() {
        cart_data = updateList;
      });
    } else {
      Fluttertoast.showToast(
        msg: "No more available products",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.yellow,
        textColor: Colors.black,
        fontSize: 16.0,
        webPosition: "center",
        webBgColor: "#ffbd40");
    };
  }

  reduceProductQuantity(ProductEntity product) {
    List<ProductEntity> updateList = cart_data;

    int index = cart_data.indexWhere((element) => element.id == product.id);
    updateList.removeAt(index);
    storage.setItem('cart', ProductEntity.productEntityToJson(updateList));
    setState(() {
      cart_data = updateList;
    });
  }

  removeProduct(id) {
    List<ProductEntity> updateList = cart_data;
    updateList.removeWhere((element) => element.id == id);
    storage.setItem('cart', ProductEntity.productEntityToJson(updateList));
    setState(() {
      cart_data = updateList;
    });
  }

  handleCheckout() async {
    if(isSignIn == true){
      await storage.ready;
      String? token = storage.getItem('token');
      Map<String, dynamic>? userInfo = storage.getItem('user-info');

      List<dynamic> cartList = [];
      for (var i = 0; i < cart_data.length; i++) {
        int productIndex = cartList.indexWhere((element) => element["_id"] == cart_data[i].id);
        if(productIndex > -1){
          cartList[productIndex]["available"] -= 1;
        } else {
          Map<String, dynamic> newItem = {
            "_id": cart_data[i].id,
            "available": cart_data[i].available!.toInt() - 1 
          };
          cartList.add(newItem);
        }
      }

      Map<String, dynamic> orderBody = {
        "status": "DELIVERING",
        "total": calcCartTotal(),
        "list": cart_data
      };
      FetchApiRepo().postData("/order", orderBody, {
        "Authorization": "Bearer: ${token as String}"
      }).then((value) => {
        //decrease product quantity
        for (var i = 0; i < cartList.length; i++) {
          FetchApiRepo().putData("/product/${cartList[i]["_id"]}", { "available": cartList[i]["available"] })
        },
        //clear storage
        storage.setItem("cart", "[]"),
        setState(() {
          cart_data = [];
        }),
        //alert
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Order success')),
        ),
      });
    } else {
      Fluttertoast.showToast(
        msg: "You have to sign in or sign up before checkout!",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.yellow,
        textColor: Colors.black,
        fontSize: 16.0,
        webPosition: "center",
        webBgColor: "#ffbd40");
    }
  }

  renderProductList(List<ProductEntity> data) {
    List<dynamic> result = [];
    for (var i = 0; i < data.length; i++) {
      ProductEntity product = data[i];
      int index = result.indexWhere((element) => element["product"].id ==  product.id);
      if(index > -1){
        result[index]["quantity"] += 1;
      } else {
        result.add({
          "product": product,
          "quantity": 1
        });
      }
    }
    return result;
  }

  String calcCartTotal() {
    double total = cart_data.fold(0.00, (sum, item) => sum + item.price!);
    return total.toStringAsFixed(2);
  }

  @override
  void initState() {
    super.initState();
    getCart();
    _CheckSignIn();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.yellow.shade700,
        title: Container(
          padding: const EdgeInsets.all(8.0),
          child: const Text('Your cart'),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.mic,
              ))
        ],
      ),
      body: Column(
        children: [
          const SizedBox(height: 10),
          Text(
            "Subtotal: \$${calcCartTotal()}",
            style: const TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.bold,
            ),
          ),
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(8.0),
                itemCount: renderProductList(cart_data).length,
                itemBuilder: (context, index) {
                  // print(renderProductList(cart_data));
                  final dynamic data = renderProductList(cart_data)[index];
                  final ProductEntity product = data["product"];
                  return Container(
                    child: Container(
                      margin: const EdgeInsets.all(8.0),
                      // height: 185,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Details_Sceen(
                                                products_details: product)));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.network(
                                        '${HomeUrlApi.baseUrl}/file/${product.images?[0]}',
                                        height: 120,
                                        width: 120,
                                        fit: BoxFit.contain,
                                      ),
                                      const SizedBox(width: 10),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.all(4.0),
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: 180,
                                                child: Text(
                                                  product.name as String,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 3,
                                                  textAlign: TextAlign.left,
                                                  style: const TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const Icon(
                                                Icons.attach_money_outlined,
                                                color: Colors.black,
                                                size: 18.0,
                                              ),
                                              Text(
                                                product.price.toString(),
                                                style: TextStyle(
                                                  color:
                                                      Colors.redAccent.shade700,
                                                  fontSize: 18.0,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    IconButton(
                                      icon: Icon(Icons.remove),
                                      onPressed: () {
                                        reduceProductQuantity(product);
                                      },
                                    ),
                                    SizedBox(
                                      width: 40.0,
                                      height: 40.0,
                                      child: Card(
                                        child: Center(
                                            child: Text(
                                          data["quantity"].toString(),
                                          textAlign: TextAlign.center,
                                        )),
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.add),
                                      onPressed: () {
                                        addProductQuantity(product);
                                      },
                                    ),
                                  ],
                                ),
                                IconButton(
                                  icon: const Icon(CupertinoIcons.trash,
                                      color: Colors.red),
                                  onPressed: () {
                                    removeProduct(product.id);
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: SizedBox(
                height: 50,
                width: 300,
                child: TextButton(
                    onPressed: () {
                      if(cart_data.isNotEmpty){
                        handleCheckout();
                      }
                    },
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.yellow.shade700,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                    child: Text('Proceed to checkout (' +
                        cart_data.length.toString() +
                        ' items)')),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
