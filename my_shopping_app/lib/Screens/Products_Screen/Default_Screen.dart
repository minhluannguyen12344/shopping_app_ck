import 'package:flutter/material.dart';
import 'package:my_shopping_app/Screens/Auth_Screen/Login_Screen/login_screen.dart';
import 'package:my_shopping_app/Screens/Auth_Screen/Register_Screen/register_screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Add_products.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Cart_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Beauty_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Book_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Details_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Fashion_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Categories_Screen/Laptop_Mobile_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Home_Screen.dart';
import 'package:my_shopping_app/Screens/Products_Screen/User_Screen.dart';

import 'package:my_shopping_app/models/Products.dart';

import 'components/search_screen.dart';

class Default_Screen extends StatefulWidget {
  const Default_Screen({Key? key}) : super(key: key);

  @override
  State<Default_Screen> createState() => _Default_Screen();
}

class _Default_Screen extends State<Default_Screen> {
  int currentIndex = 0;
  final Screens = [
    const Home_Screen(),
    const User_Screen(),
    const Cart_Screen(),
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/Laps&mobile': (context) => const Laptop_Mobile_Screen(),
        '/beauty': (context) => const Beauty_Screen(),
        '/book': (context) => const Book_Screen(),
        '/fashon': (context) => const Fashion_Screen(),
        '/Before_sign': (context) => const User_Screen(),
        '/Add_products': (context) => const Add_Products_Screen(),
        '/sign-in': (context) => const Login_Screen(),
        '/sign-up': (context) => const Register_Screen(),
        '/search' : (context) => const SearchScreen(),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: Colors.grey.shade200),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Screens[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.yellow.shade900,
          unselectedItemColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          onTap: (index) => setState(() => currentIndex = index),
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outlined),
              label: "User",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart_outlined),
              label: "Cart",
            ),
          ],
        ),
      ),
    );
  }
}
