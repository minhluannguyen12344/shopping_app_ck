import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:localstorage/localstorage.dart';
import 'package:my_shopping_app/Screens/Products_Screen/Cart_Screen.dart';
import 'package:my_shopping_app/configs/url_api.dart';
import 'package:my_shopping_app/models/Products.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../../../configs/res_api.dart';

class Details_Sceen extends StatefulWidget {
  final ProductEntity products_details;

  const Details_Sceen({Key? key, required this.products_details})
      : super(key: key);

  @override
  State<Details_Sceen> createState() => _Details_SceenState();
}

class _Details_SceenState extends State<Details_Sceen> {
  final LocalStorage storage = LocalStorage('localstorage_app');

  @override
  Widget build(BuildContext context) {
    Widget? add_cart_btn_state;

    var avb = widget.products_details.available;
    _add_cart() async {
      List<ProductEntity> cart_data = ProductEntity.productEntityFromJson(jsonDecode(storage.getItem('cart') ?? "[]"));

      List<ProductEntity> productList = cart_data.where((element) => element.id == widget.products_details.id).toList();

      if (productList.length < widget.products_details.available!) {
        cart_data.add(widget.products_details);
        storage.setItem('cart', ProductEntity.productEntityToJson(cart_data));
        Fluttertoast.showToast(
            msg: 'Add "${widget.products_details.name}" to cart',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0,
            webPosition: "center",
            webBgColor: "#ffbd40");
      } else {
        Fluttertoast.showToast(
            msg: "No more available products",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 2,
            backgroundColor: Colors.yellow,
            textColor: Colors.black,
            fontSize: 16.0,
            webPosition: "center",
            webBgColor: "#ffbd40");
      }
    }

    Widget add_cart_btn() {
      return OutlinedButton(
          onPressed: () {
            _add_cart();
          },
          style: OutlinedButton.styleFrom(
            primary: Colors.white,
            backgroundColor: Colors.yellow.shade700,
          ),
          child: const Text('Add to cart'));
    }

    Widget out_stock() {
      return Center(
        child: Text(
          '* Out of stock !',
          style: TextStyle(
              color: Colors.redAccent.shade700,
              fontSize: 24,
              fontWeight: FontWeight.bold),
        ),
      );
    }

    if (avb == 0) {
      setState(() {
        add_cart_btn_state = out_stock();
      });
    } else {
      setState(() {
        add_cart_btn_state = add_cart_btn();
      });
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade700,
        title: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), color: Colors.white),
          child: Row(
            children: const [
              Icon(
                Icons.search,
                color: Colors.black,
              ),
              Text(
                'Search',
                style: TextStyle(color: Colors.grey, fontSize: 20.0),
              )
            ],
          ),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.mic,
              ))
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10.0),
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.products_details.name as String,
              style:
                  const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            CarouselSlider.builder(
              options: CarouselOptions(
                viewportFraction: 0.7,
              ),
              itemCount: widget.products_details.images!.length,
              itemBuilder:
                  (BuildContext context, int itemIndex, int pageViewIndex) =>
                      Container(
                padding: EdgeInsets.all(4.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image.network(
                    '${HomeUrlApi.baseUrl}/file/${widget.products_details.images![itemIndex]}',
                    height: 300,
                    width: 200,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        'PRICE:',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        widget.products_details.price.toString(),
                        style: TextStyle(
                            color: Colors.redAccent.shade700, fontSize: 18.0),
                      ),
                      const Icon(
                        Icons.attach_money_outlined,
                        size: 18,
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        'Quantity:',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        widget.products_details.available.toString(),
                        style: const TextStyle(fontSize: 18.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Description:',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                  //color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0)),
              child: Row(
                children: [
                  Flexible(
                      child:
                          Text(widget.products_details.description as String)),
                ],
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Center(
                child:
                    SizedBox(width: double.infinity, child: add_cart_btn_state))
          ],
        ),
      ),
    );
  }
}
