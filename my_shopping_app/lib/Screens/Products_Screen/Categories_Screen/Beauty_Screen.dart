import 'package:flutter/material.dart';

import '../../../models/Products.dart';
import 'Details_Screen.dart';

class Beauty_Screen extends StatelessWidget {
  const Beauty_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade700,
        title: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), color: Colors.white),
          child: Row(

          ),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.mic,
              ))
        ],
      ),
      body: ListView.builder(
        itemCount: beauty.length,
        itemBuilder: (context, index) {
          final Products products = beauty[index];
          return GestureDetector(
            onTap: () {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) =>
              //             Details_Sceen(products_details: products)));
            },
            child: Container(
              padding: const EdgeInsets.all(8.0),
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Image(
                        image: AssetImage(products.img),
                        width: 120,
                        height: 80,
                        fit: BoxFit.fill,
                      ),
                      Flexible(
                        child: Text(
                          products.name,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                          maxLines: 3,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(125.0, 0, 0, 0),
                    child: Row(
                      children: [
                        Text(
                          products.price.toString(),
                          style: TextStyle(
                              color: Colors.redAccent.shade700,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold),
                        ),
                        const Icon(
                          Icons.attach_money_outlined,
                          color: Colors.black,
                          size: 15.0,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
