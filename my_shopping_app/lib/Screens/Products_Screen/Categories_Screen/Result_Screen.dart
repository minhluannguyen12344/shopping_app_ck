import 'package:flutter/material.dart';
import 'package:my_shopping_app/components/Product_List_Info.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:my_shopping_app/models/entity/product-entity.dart';

class Result_Screen extends StatefulWidget {
  final Map<String, dynamic> query;

  const Result_Screen({super.key, required this.query});

  @override
  _Result_Screen_State createState() => _Result_Screen_State();
}

class _Result_Screen_State extends State<Result_Screen> {
  List<ProductEntity> data = [];

  @override
  void initState() {
    super.initState();
    FetchApiRepo().fetchData('/product', widget.query).then((value) => {
          if (mounted)
            {
              setState(() {
                data = ProductEntity.productEntityFromJson(value);
              })
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.yellow.shade700,
          title: Container(
            padding: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0), color: Colors.white),
            child: Row(
              children: const [
                Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                Text(
                  'Search',
                  style: TextStyle(color: Colors.grey, fontSize: 20.0),
                )
              ],
            ),
          ),
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.mic,
                ))
          ],
        ),
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  decoration: BoxDecoration(color: Colors.yellow.shade600),
                  child: Row(
                    children: const [
                      Icon(Icons.location_on_outlined),
                      Text(
                        'Delivery to VietNam',
                        style: TextStyle(fontSize: 15.0),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: const BoxDecoration(color: Colors.white),
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "RESULTS",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 5),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          final ProductEntity product = data[index];
                          return Product_List_Info(data: product);
                        },
                      ),
                    ],
                  ),
                )
              ],
            )
          )
        );
  }
}
