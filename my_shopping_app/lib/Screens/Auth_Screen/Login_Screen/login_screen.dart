import 'package:flutter/material.dart';
import 'package:my_shopping_app/Screens/Products_Screen/User_Screen.dart';
import 'package:my_shopping_app/configs/res_api.dart';
import 'package:localstorage/localstorage.dart';

class Login_Screen extends StatefulWidget {
  const Login_Screen({Key? key}) : super(key: key);

  @override
  State<Login_Screen> createState() => _Login_Screen();
}

class _Login_Screen extends State<Login_Screen>{
  final LocalStorage storage = LocalStorage('localstorage_app');
  final myusername = TextEditingController();
  final mypassword = TextEditingController();

  postLogin() async {
    Map<String, dynamic> data = {
      "username": myusername.text,
      "password": mypassword.text
    };
    FetchApiRepo().postData("/auth/signin", data, {}).then((value) => {
      if(value is Map<String, dynamic>){
        storage.setItem('user-info', value["data"]),
        storage.setItem('token', value["token"]),
        Navigator.pushNamed(context, '/'),
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Container(
            color: Colors.grey[200],
            child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 100.0,
                      child: Text('Sign In Page', style: TextStyle(fontSize: 40, color: Colors.yellow[700], fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      width: 500.0,
                      child: TextField(
                        style: TextStyle(color: Colors.black),
                        controller: myusername,
                        obscureText: false,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.all(20.0),
                          hintText: 'Enter Your Username',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    SizedBox(
                      width: 500.0,
                      child: TextField(
                        controller: mypassword,
                        obscureText: true,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: EdgeInsets.all(20.0),
                            hintText: 'Enter Your Password',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    SizedBox(
                      width: 500.0,
                      child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(25.0),
                        color: Colors.yellow[700],
                        child: MaterialButton(
                          padding: EdgeInsets.all(20.0),
                          onPressed: () {
                            postLogin ();
                          },
                          child: Text('Sign in',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0)),
                          minWidth: MediaQuery.of(context).size.width,
                        ),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account?"),
                        SizedBox(width: 10.0),
                        GestureDetector(
                          child: Text(
                            "Sign up",
                            style: TextStyle(color: Colors.yellow[700]),
                          ),
                          onTap: () {
                            Navigator.pushNamed(context, '/sign-up');
                          },
                        ),
                      ],
                    )
                  ],
                ))),
      ),
    );
  }
}
