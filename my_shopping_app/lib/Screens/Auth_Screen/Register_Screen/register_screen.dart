import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:my_shopping_app/configs/res_api.dart';

class Register_Screen extends StatefulWidget {
  const Register_Screen({Key? key}) : super(key: key);

  @override
  State<Register_Screen> createState() => _Register_Screen();
}

class _Register_Screen extends State<Register_Screen>{

  final _formKey = GlobalKey<FormState>();
  
  final LocalStorage storage = LocalStorage('localstorage_app');
  final myusername = TextEditingController();
  final mypassword = TextEditingController();
  final myfullname = TextEditingController();

  RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');

  _postSignUp() async {
    Map<String, dynamic> data = {
      "username": myusername.text,
      "password": mypassword.text,
      "fullname": myfullname.text,
    };
    FetchApiRepo().postData("/auth/signup", data, {}).then((value) => {
      storage.setItem('user-info', value["data"]),
      storage.setItem('token', value["token"]),
      Navigator.pushNamed(context, '/'),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            color: Colors.grey[200],
            child: Padding(
                padding: const EdgeInsets.all(30.0),
                child:Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 100.0,
                          child: Text('Sign Up Page', style: TextStyle(fontSize: 40, color: Colors.yellow[700], fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          width: 500.0,
                          child: TextFormField(
                            controller: myusername,
                            obscureText: false,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return 'Your username cannot be empty';
                              }
                              if(value.length < 6){
                                return 'Your username must have at least 6 characters';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.all(20.0),
                              hintText: 'Username',
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                            ),
                          ),
                        ),
                        SizedBox(height:20.0,),
                        SizedBox(
                          width: 500.0,
                          child: TextFormField(
                            controller: myfullname,
                            obscureText: false,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return 'Your full name cannot be empty';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.all(20.0),
                                hintText: 'Full Name',
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
                            ),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        SizedBox(
                          width: 500.0,
                          child: TextFormField(
                            controller: mypassword,
                            obscureText: true,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return 'Your password cannot be empty';
                              }
                              if(!regex.hasMatch(value)){
                                return 'Your password must contain:\n'
                                    '+ At least 1 uppercase character\n'
                                    '+ At least 1 lowercase character\n'
                                    '+ At least 1 number\n'
                                    '+ At least 8 characters';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.all(20.0),
                                hintText: 'Password',
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
                            ),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        SizedBox(
                          width: 500.0,
                          child: TextFormField(
                            obscureText: true,
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return 'Please retype your password';
                              }
                              if(value != mypassword.text){
                                return 'Please retype exactly';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.all(20.0),
                                hintText: 'Confirm Password',
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
                            ),
                          ),
                        ),
                        SizedBox(height: 30.0),
                        SizedBox(
                          width:500.0,
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(25.0),
                            color: Colors.yellow[700],
                            child: MaterialButton(
                              padding: EdgeInsets.all(20.0),
                              onPressed: () {
                                if(_formKey.currentState!.validate()) {
                                  _postSignUp();
                                }
                              },
                              minWidth: MediaQuery.of(context).size.width,
                              child: const Text('Sign up', textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white, fontSize: 20.0)),
                            ),
                          ),
                        ),
                        SizedBox(height: 30.0),
                        const SizedBox(
                          child: Text(
                            "By creating an account, you agree with our Private Note",
                          ),
                        ),
                        SizedBox(height: 20.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Already have an account?"),
                            SizedBox(width: 10.0),
                            GestureDetector(
                              child: Text(
                                "Sign in",
                                style: TextStyle(color: Colors.yellow[700]),
                              ),
                              onTap: () {
                                Navigator.pushNamed(context, '/sign-in');
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ))),
      ),
    );
  }
}
